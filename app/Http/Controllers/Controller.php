<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index(){

    	$data = DB::select( DB::raw("SELECT category.`Name`,
					COUNT( DISTINCT item_category_relations.Id) count
				FROM
					`category`
				LEFT JOIN item_category_relations ON item_category_relations.categoryId = category.Id
				GROUP BY
					category.Id, category.`Name`
				ORDER BY
					count DESC"));

    	// echo "<pre>"; print_r($data);exit;

        return view("cetegory",['data_arr' => $data]);
    }
    public function category_list(){
    	$master = DB::select( DB::raw("SELECT
				            category.Id,
				            category.`Name`,
				            COUNT(
				                DISTINCT catetory_relations.ParentcategoryId
				            ) ct
				        FROM
				            `category`
				        LEFT JOIN catetory_relations ON catetory_relations.categoryId = category.Id
				        GROUP BY
				            category.Id,category.`Name`
				HAVING ct = 0"));

    	$child_category = DB::select( DB::raw("SELECT
					catetory_relations.ParentcategoryId,
					category.`Name`,
					COUNT(DISTINCT catetory_relations.categoryId) count
					FROM `category` 
					INNER JOIN catetory_relations ON catetory_relations.ParentcategoryId = category.Id
					GROUP BY catetory_relations.ParentcategoryId, category.`Name`"));

    	$child_category_arr = [];
    	if (!empty($child_category)) {
    		foreach ($child_category as $child) {
    			$child_category_arr[$child->ParentcategoryId]['count'] = $child->count;
    			$child_category_arr[$child->ParentcategoryId]['name'] = $child->Name;
    		}
    	}

    	// echo "<pre>"; print_r($child_category_arr);exit;

    	$item_dd = DB::select( DB::raw("SELECT
						category_name.Id catId,
						category_name.`Name` cat_name,
						parent_name.Id paId,
						parent_name.`Name` par_name
					FROM
						`catetory_relations`
					INNER JOIN category category_name ON catetory_relations.categoryId = category_name.Id
					INNER JOIN category parent_name ON catetory_relations.ParentcategoryId = parent_name.Id
					GROUP BY
						catId, cat_name,paId,par_name
					ORDER BY
						category_name.Id"));

    	$item_dd_arr = [];
    	if (!empty($item_dd)) {
    		foreach ($item_dd as $i => $child) {
    			$item_dd_arr[$child->paId][$i] = [
    				'catId' => $child->catId,
    				'cat_name' => $child->cat_name,
    				'par_name' => $child->par_name,
    				'count' => isset($child_category_arr[$child->catId]) ? $child_category_arr[$child->catId]['count'] : 0
    			];
    		}
    	}

    	// echo "<pre>"; print_r($item_dd_arr);exit;

        return view("cetegory_tree",['master' => $master, 'parent' => $child_category_arr, 'item_dd_arr' => $item_dd_arr]);
    }
}
