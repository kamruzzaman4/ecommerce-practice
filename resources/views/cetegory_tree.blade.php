<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

        <title>ECOMMERCE</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            .tree, .tree ul {
                margin:0;
                padding:0;
                list-style:none
            }
            .tree ul {
                margin-left:1em;
                position:relative
            }
            .tree ul ul {
                margin-left:.5em
            }
            .tree ul:before {
                content:"";
                display:block;
                width:0;
                position:absolute;
                top:0;
                bottom:0;
                left:0;
                border-left:1px solid
            }
            .tree li {
                margin:0;
                padding:0 1em;
                line-height:2em;
                color:#369;
                font-weight:700;
                position:relative
            }
            .tree ul li:before {
                content:"";
                display:block;
                width:10px;
                height:0;
                border-top:1px solid;
                margin-top:-1px;
                position:absolute;
                top:1em;
                left:0
            }
            .tree ul li:last-child:before {
                background:#fff;
                height:auto;
                top:1em;
                bottom:0
            }
            .indicator {
                margin-right:5px;
            }
            .tree li a {
                text-decoration: none;
                color:#369;
            }
            .tree li button, .tree li button:active, .tree li button:focus {
                text-decoration: none;
                color:#369;
                border:none;
                background:transparent;
                margin:0px 0px 0px 0px;
                padding:0px 0px 0px 0px;
                outline: 0;
            }
        </style>
    </head>
    <body>
        
<div class="container" style="margin-top:30px;">
    <div class="row">
        <div class="col-md-12">
            <p class="well" style="height:50px;"><strong>Category List</strong>

            </p>
            @foreach($master as $master)
            <ul id="">
                <li><a href="#">{{$master->Name}} ({{$parent[$master->Id]['count']}})</a>
                    @foreach($item_dd_arr[$master->Id] as $key => $item)
                    <ul>
                        <li>{{$item['cat_name']}} ({{$item['count']}})</li>
                        @if($item['count'] > 0)
                            @foreach($item_dd_arr[$item['catId']] as $key => $cat)
                            <li>
                                <ul>
                                    <li>{{$cat['cat_name']}} ({{isset($parent[$cat['catId']]['count']) ? $parent[$cat['catId']]['count'] : 0}})</li>

                                    @if($cat['count'] > 0)
                                        @foreach($item_dd_arr[$cat['catId']] as $key => $ch)
                                        <li>
                                            <ul>
                                                <li>{{$ch['cat_name']}} ({{isset($parent[$ch['catId']]['count']) ? $parent[$ch['catId']]['count'] : 0}})</li>
                                            </ul>
                                        </li>
                                        @endforeach
                                    @endif

                                </ul>
                            </li>
                            @endforeach
                        @endif
                    </ul>
                    @endforeach
                </li>                
            </ul>
            @endforeach
        </div>
    </div>
</div>      
    </body>
</html>

<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>

<script>
        $(document).ready(function() {
            $.fn.extend({
                treed: function (o) {                  
                  var openedClass = 'glyphicon-minus-sign';
                  var closedClass = 'glyphicon-plus-sign';
                  
                  if (typeof o != 'undefined'){
                    if (typeof o.openedClass != 'undefined'){
                        openedClass = o.openedClass;
                    }
                    if (typeof o.closedClass != 'undefined'){
                        closedClass = o.closedClass;
                    }
                  };
                  
                    //initialize each of the top levels
                    var tree = $(this);
                    tree.addClass("tree");
                    tree.find('li').has("ul").each(function () {
                        var branch = $(this); //li with children ul
                        branch.prepend("<i class='indicator glyphicon " + closedClass + "'></i>");
                        branch.addClass('branch');
                        branch.on('click', function (e) {
                            if (this == e.target) {
                                var icon = $(this).children('i:first');
                                icon.toggleClass(openedClass + " " + closedClass);
                                $(this).children().children().toggle();
                            }
                        })
                        branch.children().children().toggle();
                    });
                    //fire event from the dynamically added icon
                      tree.find('.branch .indicator').each(function(){
                        $(this).on('click', function () {
                            $(this).closest('li').click();
                        });
                      });
                    //fire event to open branch if the li contains an anchor instead of text
                    tree.find('.branch>a').each(function () {
                        $(this).on('click', function (e) {
                            $(this).closest('li').click();
                            e.preventDefault();
                        });
                    });
                    //fire event to open branch if the li contains a button instead of text
                    tree.find('.branch>button').each(function () {
                        $(this).on('click', function (e) {
                            $(this).closest('li').click();
                            e.preventDefault();
                        });
                    });
                }
            });

            //Initialization of treeviews

            $('#tree2').treed({openedClass:'glyphicon-folder-open', closedClass:'glyphicon-folder-close'});
            

            } );
    </script>
